## Prepare Raspberry Pi.

Open the raspi-config by `sudo nano raspi-config` and expand file system.

## Enabling video loop
 
	sudo apt-get install -y git
	git clone https://github.com/adafruit/pi_video_looper.git
	cd pi_video_looper
	sudo ./install.sh

### Change some configurations.
	
Copy video file to `/home/pi`.
Supported video file extensions are avi, mov, mkv, mp4, m4v

Open the configuration file and change something.

	sudo nano /boot/video_looper.ini

	file_reader = directory

	osd = false
	
Save & exit and reboot.


## Remove log messages on boot.
	
	sudo nano /boot/cmdline.txt

Add `logo.nologo quiet` at the end of line, and save & exit.

	sudo nano /etc/rc.local

Add `clear` just before the line `exit 0`.
	
And reboot.


## Add custom log at boot time.

	sudo apt-get install fbi

Create logo image and name it as `splash.png`.

Copy it to `/etc/`.


Create startup script.

	sudo nano /etc/init.d/asplashscreen

And add below.
	
	#! /bin/sh
	### BEGIN INIT INFO
	# Provides:          asplashscreen
	# Required-Start:
	# Required-Stop:
	# Should-Start:      
	# Default-Start:     S
	# Default-Stop:
	# Short-Description: Show custom splashscreen
	# Description:       Show custom splashscreen
	### END INIT INFO
	
	
	do_start () {
	
	    /usr/bin/fbi -T 1 -noverbose -a /etc/splash.png    
	    exit 0
	}
	
	case "$1" in
	  start|"")
	    do_start
	    ;;
	  restart|reload|force-reload)
	    echo "Error: argument '$1' not supported" >&2
	    exit 3
	    ;;
	  stop)
	    # No-op
	    ;;
	  status)
	    exit 0
	    ;;
	  *)
	    echo "Usage: asplashscreen [start|stop]" >&2
	    exit 3
	    ;;
	esac
	
	:

Make script executable and install it for init mode.

	sudo chmod a+x /etc/init.d/asplashscreen
	sudo insserv /etc/init.d/asplashscreen

Now reboot.

	sudo reboot

## Disable screen saver

	sudo apt-get install x11-xserver-utils
	sudo nano /etc/X11/xinit/xinitrc

Add following at the end of the file.
	
	xset s off         # don't activate screensaver
	xset -dpms         # disable DPMS (Energy Star) features.
	xset s noblank     # don't blank the video device

Change another file.
	
	sudo nano /etc/lightdm/lightdm.conf

In the `SeatDefaults` section it gives the command for starting the X server which I modified to get it to turn off the screen saver as well as dpms.

	[SeatDefaults]
	xserver-command=X -s 0 -dpms

And reboot.

	sudo reboot